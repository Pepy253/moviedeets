<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) 
        {
            $table->increments('movId');
            $table->string('movName')->unique();
            $table->integer('genre_Id')->unsigned();
            $table->bigInteger('user_Id')->unsigned();
            $table->smallInteger('movYear');
            $table->smallInteger('movDuration');
            $table->string('movImage');
            $table->timestamps();

            $table
            ->foreign('user_Id')
            ->references('id')->on('users')->onDelete('cascade');
            $table
            ->foreign('genre_id')
            ->references('genId')->on('genres')->onDelete('cascade');
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
