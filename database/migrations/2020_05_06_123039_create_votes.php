<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->integer('movie_Id')->unsigned();
            $table->bigInteger('user_Id')->unsigned();
            $table->timestamps();

            $table->primary(['movie_Id', 'user_Id']);

            $table
            ->foreign('movie_id')
            ->references('movId')->on('movies')->onDelete('cascade');
            $table
            ->foreign('user_id')
            ->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
