@extends('layouts.master')

@section('title', 'Početna')

@section('content')
<section class="hero-area" id="home">
		<div class="container">
			<div class="panel panel-default">
				<div class="hero-area-slider">
				@inject('provider', 'App\Http\Controllers\MovieController')
					@for($i=0;$i<=2;$i++)
						<div class="row hero-area-slide">
							<div class="col-lg-6 col-md-5">
								<div class="hero-area-content">
									<img src="{{ asset('uploads/' . $movies[$i]->movImage) }}" alt="about" style="width:430px;height:594px; margin-bottom: 3em;" />
								</div>
							</div>
							<div class="col-lg-6 col-md-7">
								<div class="hero-area-content pr-50">
								<a href="{{ action('MovieController@view', $movies[$i]->movId) }}" style="font-size: 30px;">{{ $movies[$i]->movName }}</a>
									<div class="review" style="margin: 8px 0 20px;">
										<div class="author-review" >
										@for($j=0;$j<=min($provider->getCount($movies[$i]->movId)/5,4);$j++)
												<i class="icofont icofont-star"></i>
											@endfor
										</div>
										<h4><h4>{{ $provider->getCount($movies[$i]->movId) }} glasova</h4>
											<h4 style="padding-left: 40px; margin-top: .20em;">Godina: &nbsp{{ $movies[$i]->movYear }}</h4>
											<h4 style="padding-left: 40px; margin-top: .20em;">Trajanje: &nbsp{{ $movies[$i]->movDuration }} min</h4>
									</div>
									<p>{{ $movies[$i]->description }}</p> 
								</div>
								<h4 style="margin-top: 2em; margin-bottom: 2em; font-size: 15px; color: #ff82ab;"><--Povuci za prethodni film  &nbsp&nbsp&nbsp Povuci za slijedeći film--></h4>
							</div>
						</div>
					@endfor
					</div>
					<div class="hero-area-thumb">
					@for($l=1;$l<=2;$l++)
						@if($l==1)
						<div class="thumb-prev">
						@else
						<div class="thumb-next">
						@endif	
							<div class="row hero-area-slide">
								<div class="col-lg-6">
									<div class="hero-area-content">
										<img src="{{ asset('uploads/' . $movies[$l]->movImage) }}" alt="about" style="width:430px;height:594px;" />
									</div>
								</div>
								<div class="col-lg-6">
									<div class="hero-area-content pr-50">
									<a href="{{ action('MovieController@view', $movies[$l]->movId) }}" style="font-size: 20px;">{{ $movies[$l]->movName }}</a>
										<div class="review">
											<div class="author-review">
											@for($j=0;$j<=min($provider->getCount($movies[$l]->movId)/5,5);$j++)
												<i class="icofont icofont-star"></i>
											@endfor
											</div>
											<h4>{{ $provider->getCount($movies[$l]->movId) }} glasova</h4>
											<h4 style="padding-left: 40px; margin-top: .20em;">Godina: &nbsp{{ $movies[$l]->movYear }}</h4>
											<h4 style="padding-left: 40px; margin-top: .20em;">Trajanje: &nbsp{{ $movies[$l]->movDuration }} min</h4>
										</div>
									</div>
								</div>
							</div>	
						</div>
					@endfor
			</div>
		</div>
</section>
@endsection




