<!DOCTYPE HTML>
<html lang="hr">
    <head>
        @include('includes.head')
        <title>MovieDeets - @yield('title')</title>
    </head>
    
    <body>
        <div id="preloader"></div>
            <header class="header">
                @include('includes.vHeader')
            </header>

            @yield('content')

            @yield('ocasional')

            @include('includes.video')
            
            <footer class="footer">
                @include('includes.footer')
            </footer>
            
        @include('includes.js')
    </body>
</html>