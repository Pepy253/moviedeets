@extends('layouts.vMaster')

@section('title', 'Uredi film')

@section('content')
<section class="breadcrumb-area">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="breadcrumb-area-content">
            @include('common.errors')
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>						
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header" style="background-color: #10131c;">Uredi film</div>

                <div class="card-body" style="background-color: #13151f;">
                    <form method="POST" action="{{ action('MovieController@edit', $movie->movId ) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="movName">Naziv filma:</label>                            
                                <input id="movName" type="text" class="form-control" name="movName" value="{{ old('movie') ? old('movie') : $movie->movName }}" style="background-color: 	#f0ffff;">
                        </div>

                        <div class="form-group row">
                            <label for="genre">Žanr: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                            <div class="col-md-6">                  
                                <select name="genre" id="genre" class="form-control" style="background-color: 	#f0ffff;">
                                    @foreach($genres as $genre)
                                        @if($genre->genId == $movie->genre_Id)
                                            <option value="{{ $genre->genId }}"  selected>{{ $genre->genName }}</option>
                                        @else
                                            <option value="{{ $genre->genId }}">{{ $genre->genName }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="movYear">Year: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                            <div class="col-md-6">
                                <select name="movYear" class="form-control" style="background-color: 	#f0ffff;">
                                    @foreach($years as $year)
                                        @if($year == $movie->movYear)
                                        <option value="{{ $year }}" selected>{{ $year }}</option>
                                        @else
                                        <option value="{{ $year }}" >{{ $year }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="movDur">Trajanje:</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="movDur" id="movDur" value="{{ old('movie') ? old('movie') : $movie->movDuration }}" style="background-color: 	#f0ffff;">
                            </div>
                        </div>

                        <<div class="form-group row">
                            <label for="trailer">Trailer link:</label>                            
                                <input id="trailer" type="text" class="form-control" name="trailer" value="{{ old('movie') ? old('movie') : $movie->trailer }}" style="background-color: 	#f0ffff;">
                        </div>

                        <<div class="form-group row">
                            <label for="descritpion">Opis:</label>                            
                                <input id="description" type="textarea" class="form-control" name="description" value="{{ old('movie') ? old('movie') : $movie->description }}" style="background-color: 	#f0ffff;">
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-4">
                                <button type="submit" class="btn btn-light" style="background-color: 	#f0ffff;">
                                    Uredi film
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection

