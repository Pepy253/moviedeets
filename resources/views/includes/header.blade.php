			<div class="container">
				<div class="header-area">
					<div class="logo">
						<a href="{{ action('HomeController@index') }}"><img src="assets/img/logo1.png" alt="logo" style="width:150px;height:100px;" /></a>
					</div>
					<div class="header-right" style="display: block;">
						<ul>
						@if (Route::has('login'))
		                    @auth
								<li>{{ Auth::user()->name }}</li>
								<li><a href="{{ route('logout') }}"
								onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Odjava
									</a>
								</li>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                    		@else
								<li><a class="login" href="login">Prijava</a></li>
									@if (Route::has('register'))
                            			<li><a class="register" href="register">Registracija</a></li>
                        			@endif
                    		@endauth
						@endif
						</ul>
					</div>
					<div class="menu-area">
						<div class="responsive-menu"></div>
					    <div class="mainmenu">
                            <ul id="primary-menu">
                                <li><a href="{{ action('HomeController@index') }}">Početna</a></li>
                                <li><a href="{{ action('MovieController@index') }}">Filmovi</a></li>
                                <li><a href="{{ action('MovieController@myMovies') }}">Moji filmovi</a></li>
                                <li><a href="{{ action('MovieController@add') }}"><i class="icofont icofont-plus"></i> Dodaj film</a></li>
                            </ul>
					    </div>
					</div>
				</div>
			</div>
		</header>
