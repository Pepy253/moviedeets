		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="{{ URL::asset('assets/img/logo1.png') }}" rel="icon" type="image/pgn" >
		<link href="{{ URL::asset('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"  media="all" >
		<link href="{{ URL::asset('/css/slicknav.min.css') }}" rel="stylesheet" type="text/css"  media="all" >
		<link href="{{ URL::asset('/css/icofont.css') }}" rel="stylesheet" type="text/css"  media="all" >
		<link href="{{ URL::asset('/css/owl.carousel.css') }}" rel="stylesheet" type="text/css" >
		<link href="{{ URL::asset('/css/magnific-popup.css') }}" rel="stylesheet" type="text/css" >
		<link href="{{ URL::asset('/css/style.css') }}" rel="stylesheet" type="text/css"  media="all" >
		<link href="{{ URL::asset('/css/responsive.css') }}" rel="stylesheet" type="text/css"  media="all" >
		<meta name="csrf-token" content="{{ csrf_token() }}">
	