@inject('provider', 'App\Http\Controllers\MovieController')
	<section class="video ptb-90">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="section-title pb-20">
							<h1><i class="icofont icofont-film"></i> Traileri novo dodanih filmova</h1>
						</div>
					</div>
				</div>
				<hr />
				<div class="row">
                    <div class="col-md-9">
						<div class="video-area">
							<img src="{{ asset('uploads/' . $movies[0]->movImage) }}" alt="video" style="width:868px;height:425px;" />
							<a href="{{ $movies[0]->trailer }}" class="popup-youtube">
								<i class="icofont icofont-ui-play"></i>
							</a>
							<div class="video-text">
								<h2>{{ $movies[0]->movName }}</h2>
								<div class="review">																		
									<div class="author-review">
									@for($i=0;$i<=min($provider->getCount($movies[0]->movId)/5,5);$i++)
											<i class="icofont icofont-star"></i>
										@endfor
									</div>
									<h4>{{ $provider->getCount($movies[0]->movId) }} votes</h4>
								</div>
							</div>
						</div>
                    </div>
                    <div class="col-md-3">
						<div class="row">
							<div class="col-md-12 col-sm-6">
								<div class="video-area">
									<img src="{{ asset('uploads/' . $movies[1]->movImage) }}" alt="video"  style="width:255px;height:182.83px;" />
									<a href="{{ $movies[1]->trailer }}" class="popup-youtube">
										<i class="icofont icofont-ui-play"></i>
									</a>
								</div>
							</div>
							<div class="col-md-12 col-sm-6">
								<div class="video-area">
									<img src="{{ asset('uploads/' . $movies[2]->movImage) }}" alt="video" style="width:255px;height:182.83px;" />
									<a href="{{ $movies[2]->trailer }}" class="popup-youtube">
										<i class="icofont icofont-ui-play"></i>
									</a>
								</div>
							</div>
						</div>
                    </div>
				</div>
			</div>
		</section>
