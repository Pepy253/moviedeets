		<script src="{{ asset('/js/jquery.min.js') }}"></script>
		<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('/js/jquery.slicknav.min.js') }}"></script>
		<script src="{{ asset('/js/owl.carousel.min.js') }}"></script>
		<script src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
		<script src="{{ asset('/js/isotope.pkgd.min.js') }}"></script>
		<script src="{{ asset('/js/main.js') }}"></script>