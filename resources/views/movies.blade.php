@extends('layouts.master')

@section('title', 'Lista svih filmova')

@section('content')
<section class="breadcrumb-area">
			<div class="container">
                <div class="row">
					<div class="col-lg-12">
						<div class="breadcrumb-area-content">
						@if (\Session::has('success'))
							<div class="alert alert-success">
								<ul>						
									<li>{!! \Session::get('success') !!}</li>
								</ul>
							</div>
						@elseif (\Session::has('error'))
							<div class="alert alert-danger">
								<ul>
									<li>{!! \Session::get('error') !!}</li>
								</ul>
							</div>
						@endif					
						</div>
                    </div>
                </div>    
			</div>
		</section>
		@inject('provider', 'App\Http\Controllers\MovieController')
		<section class="portfolio-area pt-60">
                <div class="container">				
					<div class="row flexbox-center">
						
					    <div class="section-title">
							<h1><i class="icofont icofont-movie"></i> Lista filmova</h1>
                        </div>
                    <div class="col-lg-10 text-center text-lg-right" style="text-align: center!important;">
					    <div class="portfolio-menu">
							<ul>
                                @foreach($letters as $letter)
								<li data-filter=".{{ $letter }}">{{ $letter }}</li>
								@endforeach
							</ul>
						</div>
					</div>					
				</div>
				<hr />
				<div class="row">
					<div class="col-lg-12">
						<div class="row portfolio-item">
                            @foreach($movies as $movie)
							<div class="col-lg-3 col-md-4 col-sm-6 {{ $provider->getLetter($movie->movName) }}">
								<div class="single-portfolio">
									<div class="single-portfolio-img">
										<img src="{{ asset('uploads/' . $movie->movImage) }}" alt="portfolio" style="width:255px;height:425px;" />
										<a href="{{ $movie->trailer }}" class="popup-youtube">
											<i class="icofont icofont-ui-play"></i>
										</a>
									</div>
									<div class="portfolio-content">
										<a href="{{ action('MovieController@view', $movie->movId ) }}" style="font-size: 22px;">{{ $movie->movName }}</a>
										<div class="review">
											<h5 style="font-size: 16px;">{{ $movie->movYear }}, &nbsp{{ $movie->movDuration }} min<h5>												
											<div class="author-review">
											@for($i=0;$i<=min($provider->getCount($movie->movId)/5,4);$i++)
												<i class="icofont icofont-star"></i>
											@endfor
											</div>
											<h4 style="font-size: 16px;">{{ $provider->getCount($movie->movId) }} glasova</h4>
											<form style="margin-top: 1em; text-align: right;" action="{{ action('MovieController@destroy', $movie->movId ) }}" method="POST">
														<button type="submit" class="btn btn-danger" style="background-color: 	#ff3030; color: #f8f8ff; padding: .375rem .75rem;"><i class="icofont icofont-delete"></i> Obriši</button>
														{{ csrf_field() }}
													{{ method_field('DELETE') }}
											</form>
											<a class="btn btn-success" style="margin-top: -4em;" href="{{ action('MovieController@edit', $movie->movId) }}"><i class="icofont icofont-edit"></i> Uredi</a> 
										</div>
									</div>
								</div>
                            </div>
                            @endforeach			
								</div>										
							</div>																															
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</section>
@endsection

