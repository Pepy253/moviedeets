@extends('layouts.vMaster')

@section('title', $mov->first()->movName)

@section('content')
@inject('provider', 'App\Http\Controllers\MovieController')
<section class="breadcrumb-area" style="background-image: url(../uploads/{{ $mov->first()->movImage }});">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="breadcrumb-area-content">
							<h1>{{ $mov->first()->movName }}</h1>
							@if (\Session::has('success'))
							<div class="alert alert-success">
								<ul>						
									<li>{!! \Session::get('success') !!}</li>
								</ul>
							</div>
						@elseif (\Session::has('error'))
							<div class="alert alert-danger">
								<ul>
									<li>{!! \Session::get('error') !!}</li>
								</ul>
							</div>
						@endif			
						</div>
					</div>
				</div>
			</div>
		</section><!-- breadcrumb area end -->
		<!-- transformers area start -->
		<section class="transformers-area">
			<div class="container">
				<div class="transformers-box">
					<div class="row flexbox-center">
						<div class="col-lg-5 text-lg-left text-center">
							<div class="transformers-content">
								<img src="{{ asset('/uploads/' . $mov->first()->movImage) }}" alt="about" style="width:415px;height:575px;" />
							</div>
						</div>
						<div class="col-lg-7">
							<div class="transformers-content">
								<h2>{{ $mov->first()->movName }}</h2>
								<p>{{ $mov->first()->genName }}</p>
								<ul>
									<li>
										<div class="transformers-left">
											Žanr:
										</div>
										<div class="transformers-right">
											{{ $mov->first()->genName }}
										</div>
									</li>
									<li>
										<div class="transformers-left">
											Trajanje: 
										</div>
										<div class="transformers-right">
											{{ $mov->first()->movDuration }} min
										</div>
									</li>
									<li>
										<div class="transformers-left">
											Godina:
										</div>
										<div class="transformers-right">
											{{ $mov->first()->movYear }}
										</div>
									</li>
									<li>
										<div class="transformers-left">
											Ocjena:
										</div>
										<div class="transformers-right">
                                        @for($i=0;$i<=min($provider->getCount($mov->first()->movId)/5,4);$i++)
												<i class="icofont icofont-star" style="color: #fece50;"></i>
										@endfor
                                        {{ $provider->getCount($mov->first()->movId) }} glasova
										</div>
									</li>
									<li>
										<div class="transformers-left">
										</div>
										<div class="transformers-right">
                                        
											<div class="transformers-bottom">
                                            <a class="btn btn-success" style="height: 42px; border: none; color: #212529;" href="{{ action('MovieController@edit', $mov->first()->movId) }}"><i class="icofont icofont-edit"></i> Uredi</a> 
											<form action="{{ action('MovieController@vote', $mov->first()->movId ) }}" method="POST">
														<button type="submit" style="background-color: #ffc125; padding: 8px 15px; color: #212529;" class="btn btn-warning"><i class="icofont icofont-thumbs-up"></i> Glasaj</button>
														{{ csrf_field() }}
											</form>
											<form action="{{ action('MovieController@destroy', $mov->first()->movId ) }}" method="POST">
														<button type="submit" class="btn btn-danger" style="background-color:  #dc3545; padding: 8px 15px; color: #212529;"><i class="icofont icofont-delete"></i> Obriši</button>
														{{ csrf_field() }}
													{{ method_field('DELETE') }}
											</form>
											</div>
											
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- transformers area end -->
		<!-- details area start -->
		<section class="details-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-9">
						<div class="details-content">
							<div class="details-overview">
								<h2>Opis:</h2>
								<p>{{ $mov->first()->description }}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>