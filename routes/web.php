<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function() {

    
    Route::auth();

    
    Route::get('/', function() { return view('welcome'); });

        
        Route::get('/movies', 'MovieController@index');

        Route::get('/myMovies', 'MovieController@myMovies');
        
        Route::delete('/movies/{id}', 'MovieController@destroy');
        
        Route::get('/add', 'MovieController@add');
        Route::post('/add', 'MovieController@save');
        
        Route::get('/view/{movId}', 'MovieController@view');
        Route::post('/view/{movId}', 'MovieController@vote');

        Route::get('/edit/{movId}', 'MovieController@edit');
        Route::post('/edit/{movId}', 'MovieController@update');

        

});

Route::get('/home', 'HomeController@index')->name('home');
