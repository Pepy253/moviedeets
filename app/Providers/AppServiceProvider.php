<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Movie;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        view()->composer('*', function (View $view){
            $movies = DB::table('movies')->orderBy('created_at', 'desc')->get();

            $view->with('movies', $movies);
        });

        view()->composer('*', function (View $view){
            $letters = range('A', 'Z');

            $view->with('letters', $letters);
        });
    }
}
