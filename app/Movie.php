<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $primaryKey = 'movId';

    protected $table = 'movies';

    public function genres()
    {
        $this->belongsTo(Genre::class);
    }

    public function users()
    {
        $this->belongsTo(User::class);
    }

}
