<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Genre;
use App\Movie;
use App\Vote;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;



class MovieController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {             
        return view('movies');
    }

    public function add()
    {
        $movOrder = DB::table('movies')->orderBy('movName', 'asc')->get();
        
        $years = [];
        for($year = 1900; $year <= date("Y"); $year++)
        {
            $years[$year] = $year;
        }
        
        
        return view('add')->with('genres', Genre::get())->with('years', $years)->with('ordMovies', $movOrder);
    }

    public function save(Request $request)
    {
        $data = $request->all();

        $customMessages = array(
            'movName.required' => 'Naziv filma je obvezan!',
            'movName.max' => 'Naziv filma može maksimalno 30 znakova!',
            'movName.unique' => 'Film ' . $data['movName'] . ' već postoji!',
            'movDur.required' => 'Polje Trajanje je obvezno!',
            'movDur.max' => 'Vrijednost polja Trajanje ne može biti veća od 320!',
            'movDur.numeric' => 'Polje Trajanje može sadržavati isključivo brojeve!',
            'trailer.required' => 'Polje Trailer link je obvezno!',
            'description.required' => 'Polje Opis je obvezno!',
        );
        
        $this->validate($request, [
            'movName' => 'required|max:30|unique:movies',
            'movDur' => 'required|numeric|max:320',
            'trailer' => 'required',
            'description' => 'required',
            ], $customMessages);

        
        $newMovie = new Movie;
        $newMovie->movName = $data['movName'];
        $newMovie->genre_Id = $data['genre'];
        $newMovie->user_Id = auth()->user()->id;
        $newMovie->movYear = $data['movYear'];
        $newMovie->movDuration = $data['movDur'];
        $newMovie->trailer = $data['trailer'];
        $newMovie->description = $data['description']; 

        if($request->hasFile('movImg'))
        {
            $file = $request->file('movImg');
            $destinationPath = '../public/uploads';
            $fileNameWithExt = $file->getClientOriginalName();
            $checkIfExists = $destinationPath . "/" . $fileNameWithExt;
            if(file_exists($checkIfExists))
            {
                return redirect()->action('MovieController@add')->with('fail', 'Slika pod tim imenom već postoji!!');
            }
            else
            {
                $file->move($destinationPath, $fileNameWithExt);
            }

        }
        else
        {
            $fileNameWithExt = 'No Image';
        }

        $newMovie->movImage = $fileNameWithExt;

        $newMovie->save();
    
        return redirect()->action('MovieController@add')->with('success', 'Film je uspješno spremljen!');
    }

    public function getCount($movId)
    {
        $count = DB::table('votes')
                    ->where('movie_Id', $movId)
                    ->count();

        if(empty($count))
        {
            return $count = 0;
        }
        else
        {
            return $count;
        }
    }

    public function getLetter($string)
    {
        $letters = range('A', 'Z');

        foreach($letters as $letter)
        {
            if (preg_match('#^'.$letter.'#', $string) === 1) 
            {
                return  $letter;
            }
        }
    }

    public function myMovies()
    {
        $user = auth()->user()->id;

        $myMovies = DB::table('movies')->where('user_Id', 'like', $user)->get();

        return view('/myMovies')->with('myMovies', $myMovies);
    }

    public function view($movId)
    {
        return view('/view')->with('mov', DB::table('movies')->join('genres', 'movies.genre_Id', '=', 'genres.genId')->where('movId', 'like', $movId)->get());
    }

    public function destroy(Request $request, $movId)
    {
        $q = Movie::find($movId);

        $movieName = $q->movName;

        $movOwner = $q->user_Id;

        $userId = auth()->user()->id; 
        
            if($userId == $q->user_Id)
            {   
                $fileToDelete = "../public/uploads";
                $fileToDelete .= "/";
                $fileToDelete .= $q->movImage;
                
                if(file_exists($fileToDelete))
                {
                    unlink($fileToDelete);
                }

                $movie = Movie::where('movId', '=', $movId )->delete();

                return redirect()->action('MovieController@index')->with('success', 'Film ' . $movieName . ' je uspješno izbrizan!!');
            }
            else
            {
                return redirect()->action('MovieController@index')->with('error', 'Nemoguće obrisati film jer niste vlasnik!!' );
            }     
    }

    public function vote($movId)
    {
        $user = auth()->user()->id;

        $newVote = new Vote;
        $newVote->movie_Id = $movId;
        $newVote->user_Id = $user;
        
        try {
            $newVote->save();

            return redirect()->action('MovieController@view', $movId)->with('success', 'Glas uspješno dodan!');
        } catch(\Illuminate\Database\QueryException $ex){ 

            return redirect()->action('MovieController@view', $movId)->with('error', 'Vaš glas za ovaj film već postoji!');
        }
       
    }

    public function edit($movId)
    {
        $user = auth()->user()->id;

        $movie = Movie::find($movId);

        $years = [];
        for($year = 1900; $year <= date("Y"); $year++)
        {
            $years[$year] = $year;
        }

        if($user == $movie->user_Id)
        {
            return view('edit')->with('movie', $movie)->with('genres', Genre::get())->with('years', $years);
        }
        else
        {
            return redirect()->action('MovieController@index')->with('error', 'Nije moguće urediti film jer niste vlasnik!');
        }

    }

    public function update(Request $request, $movId)
    {
        $data = $request->all();
        $movUp = Movie::find($movId);

        $customMessages = array(
            'movName.required' => 'Naziv filma je obvezan!',
            'movName.max' => 'Naziv filma može maksimalno 30 znakova!',
            'movDur.required' => 'Polje Trajanje je obvezno!',
            'movDur.max' => 'Vrijednost polja Trajanje ne može biti veća od 320!',
            'movDur.numeric' => 'Polje Trajanje može sadržavati isključivo brojeve!',
            'trailer.required' => 'Polje Trailer link je obvezno!',
            'description.required' => 'Polje Opis je obvezno!',
        );
        
        $this->validate($request, [
            'movName' => 'required|max:30',
            'movDur' => 'required|numeric|max:320',
            'trailer' => 'required',
            'description' => 'required',
            ], $customMessages);


        $movUp->movName = $data['movName'];
        $movUp->genre_Id = $data['genre'];
        $movUp->movYear = $data['movYear'];
        $movUp->movDuration = $data['movDur'];
        $movUp->trailer = $data['trailer'];
        $movUp->description = $data['description'];

        if($movUp->save())
        {
            return redirect()->action('MovieController@index')->with('success', 'Film uspješno uređen!');
        }
        else
        {
            return redirect()->action('MovieController@index')->with('error', 'Uređivanje nije uspjelo!');
        }  
    }
    

}
